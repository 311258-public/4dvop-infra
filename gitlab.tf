resource "docker_image" "gitlab" {
  name = "gitlab/gitlab-ce:13.10.1-ce.0"
}

resource "docker_volume" "gitlab-config" {
  name = "gitlab-config"
}

resource "docker_volume" "gitlab-logs" {
  name = "gitlab-logs"
}

resource "docker_volume" "gitlab-data" {
  name = "gitlab-data"
}

resource "docker_container" "gitlab" {
  image = docker_image.gitlab.latest
  name  = "gitlab"
  env = [ <<EOF
GITLAB_OMNIBUS_CONFIG=external_url 'https://git.northamp.local'
nginx['listen_port'] = 80
nginx['listen_https'] = false
nginx['http2_enabled'] = false

nginx['proxy_set_headers'] = {
  "Host" => "$http_host",
  "X-Real-IP" => "$remote_addr",
  "X-Forwarded-For" => "$proxy_add_x_forwarded_for",
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}

gitlab_rails['gitlab_shell_ssh_port'] = 22

registry_external_url 'https://registry.git.northamp.local'
registry_nginx['listen_port'] = 5100
registry_nginx['listen_https'] = false

registry_nginx['proxy_set_headers'] = {
  "Host" => "$http_host",
  "X-Real-IP" => "$remote_addr",
  "X-Forwarded-For" => "$proxy_add_x_forwarded_for",
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}

gitlab_pages['enable'] = false

gitlab_rails['time_zone'] = 'Europe/Paris'
# Reduce the number of running workers to the minimum in order to reduce memory usage
puma['worker_processes'] = 1
sidekiq['concurrency'] = 5

# Further puma tuning
puma['min_threads'] = 1
puma['max_threads'] = 1

# Turn off monitoring to reduce idle cpu and disk usage
prometheus_monitoring['enable'] = false
    EOF
  ]
  volumes {
    volume_name = "gitlab-config"
    container_path = "/etc/gitlab"
  }
  volumes {
    volume_name = "gitlab-logs"
    container_path = "/var/log/gitlab"
  }
  volumes {
    volume_name = "gitlab-data"
    container_path = "/var/opt/gitlab"
  }
  networks_advanced {
    name = "web"
  }

  labels {
    label = "traefik.enable"
    value = true
  }
  labels {
    label = "traefik.http.routers.gitlab.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.gitlab.rule"
    value = "host(`git.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.gitlab.service"
    value = "gitlab"
  }
  labels {
    label = "traefik.http.routers.gitlab-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.gitlab-ssl.rule"
    value = "host(`git.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.gitlab-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.gitlab-ssl.service"
    value = "gitlab"
  }
  labels {
    label = "traefik.http.services.gitlab.loadBalancer.server.port"
    value = "80"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry.rule"
    value = "host(`registry.git.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry.service"
    value = "gitlabregistry"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry-ssl.rule"
    value = "host(`registry.git.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.gitlabregistry-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.gitlabregistry-ssl.service"
    value = "gitlabregistry"
  }
  labels {
    label = "traefik.http.services.gitlabregistry.loadBalancer.server.port"
    value = "5100"
  }
}
