locals {
  prometheus-config = <<-EOT
  global:
    # How frequently to scrape targets by default.
    scrape_interval: 10s
  
    # How long until a scrape request times out.
    # [ scrape_timeout: <duration> | default = 10s ]
  
    # How frequently to evaluate rules.
    # [ evaluation_interval: <duration> | default = 1m ]
  
    # The labels to add to any time series or alerts when communicating with
    # external systems (federation, remote storage, Alertmanager).
    external_labels:
      env: 'pozos'
  
    # File to which PromQL queries are logged.
    # Reloading the configuration will reopen the file.
    # [ query_log_file: <string> ]
  
  # Rule files specifies a list of globs. Rules and alerts are read from
  # all matching files.
  # rule_files:
  #  [ - <filepath_glob> ... ]
  
  # A list of scrape configurations.
  scrape_configs:
    # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
    - job_name: 'prometheus'
  
      # Override the global default and scrape targets from this job every 5 seconds.
      scrape_interval: 5s
  
      static_configs:
        - targets: ['localhost:9090']

    - job_name: 'host'
      static_configs:
        - targets: ['172.27.0.1:9100']

    - job_name: 'docker'
      static_configs:
        - targets: ['172.27.0.1:9323']

  # Alerting specifies settings related to the Alertmanager.
  # alerting:
  #   alert_relabel_configs:
  #     [ - <relabel_config> ... ]
  #   alertmanagers:
  #     [ - <alertmanager_config> ... ]
  
  # Settings related to the remote write feature.
  # remote_write:
  #   [ - <remote_write> ... ]
  
  # Settings related to the remote read feature.
  # remote_read:
  #   [ - <remote_read> ... ]
  EOT
}
