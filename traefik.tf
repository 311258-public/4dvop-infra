resource "docker_image" "traefik" {
  name = "traefik:v2.4.8"
}

resource "docker_image" "socket-proxy" {
  name = "tecnativa/docker-socket-proxy:latest"
}

resource "docker_container" "traefik" {
  image = docker_image.traefik.latest
  name  = "traefik"
  networks_advanced {
    name = "web"
  }
  networks_advanced {
    name = "socket-private"
  }
  command = [
    "--providers.docker=true",
    "--providers.docker.network=web",
    "--providers.docker.exposedbydefault=false",
    "--entryPoints.web.address=:80",
    "--entryPoints.web-secure.address=:443",
    "--accesslog=true",
    "--providers.docker.endpoint=tcp://socket-proxy:2375",
    "--api=true",
    "--serverstransport.insecureskipverify=true"
  ]
  ports {
    internal = 80
    external = 80
  }
  ports {
    internal = 443
    external = 443
  }
  labels {
    label = "traefik.enable"
    value = "true"
  }
  labels {
    label = "traefik.http.routers.api.rule"
    value = "Host(`traefik.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.api.service"
    value = "api@internal"
  }
}

resource "docker_container" "socket-proxy" {
  image = docker_image.socket-proxy.latest
  name = "socket-proxy"
  networks_advanced {
    name = "socket-private"
  }
  volumes {
    host_path = "/var/run/docker.sock"
    container_path = "/var/run/docker.sock"
  }
  env = [
    "NETWORKS=1",
    "CONTAINERS=1",
    "TASKS=1",
    "BUILD=1",
    "IMAGES=1",
    "POST=1",
    "EXEC=1"
  ]
}
