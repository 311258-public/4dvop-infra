resource "docker_image" "prometheus" {
  name = "prom/prometheus:v2.26.0"
}

resource "docker_image" "node-exporter" {
  name = "quay.io/prometheus/node-exporter:v1.1.2"
}

resource "local_file" "prometheus-config" {
  content = local.prometheus-config
  filename = "/srv/prometheus/prometheus.yml"
  file_permission = "0755"
}

resource "docker_volume" "prometheus-data" {
  name = "prometheus-data"
}

resource "docker_container" "prometheus" {
  image = docker_image.prometheus.latest
  name  = "prometheus"

  volumes {
    host_path = "/srv/prometheus/prometheus.yml"
    container_path = "/etc/prometheus/prometheus.yml"
    read_only = true
  }
  volumes {
    volume_name = "prometheus-data"
    container_path = "/prometheus"
  }
  networks_advanced {
    name = "web"
  }

  labels {
    label = "traefik.enable"
    value = true
  }
  labels {
    label = "traefik.http.routers.prometheus.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.prometheus.rule"
    value = "host(`prometheus.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.prometheus.service"
    value = "prometheus"
  }
  labels {
    label = "traefik.http.routers.prometheus-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.prometheus-ssl.rule"
    value = "host(`prometheus.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.prometheus-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.prometheus-ssl.service"
    value = "prometheus"
  }
  labels {
    label = "traefik.http.services.prometheus.loadBalancer.server.port"
    value = "9090"
  }
}

resource "docker_container" "node-exporter" {
  image = docker_image.node-exporter.latest
  name  = "node-exporter"
  pid_mode = "host"
  network_mode = "host"

  volumes {
    host_path = "/"
    container_path = "/host"
    read_only = true
  }
}
