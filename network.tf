resource "docker_network" "web" {
  name = "web"
}

resource "docker_network" "socket-private" {
  name = "socket-private"
  # This is preferred, but blocks web connections between Traefik and the socket proxy for some reason.
  # internal = true
}