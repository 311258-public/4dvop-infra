resource "docker_image" "grafana" {
  name = "grafana/grafana:7.5.3"
}

resource "docker_volume" "grafana-data" {
  name = "grafana-data"
}

resource "docker_volume" "grafana-logs" {
  name = "grafana-logs"
}

resource "docker_container" "grafana" {
  image = docker_image.grafana.latest
  name  = "grafana"

  volumes {
    volume_name = "grafana-data"
    container_path = "/var/lib/grafana"
  }
  volumes {
    volume_name = "grafana-logs"
    container_path = "/var/log/grafana"
  }
  networks_advanced {
    name = "web"
  }

  labels {
    label = "traefik.enable"
    value = true
  }
  labels {
    label = "traefik.http.routers.grafana.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.grafana.rule"
    value = "host(`grafana.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.grafana.service"
    value = "grafana"
  }
  labels {
    label = "traefik.http.routers.grafana-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.grafana-ssl.rule"
    value = "host(`grafana.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.grafana-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.grafana-ssl.service"
    value = "grafana"
  }
  labels {
    label = "traefik.http.services.grafana.loadBalancer.server.port"
    value = "3000"
  }

  env = [
    "GF_SECURITY_ADMIN_PASSWORD=password",
    "GF_SERVER_ROOT_URL=https://grafana.northamp.local",
    "GF_USERS_ALLOW_SIGN_UP=false"
  ]
}
