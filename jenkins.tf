resource "docker_image" "jenkins" {
  name = "jenkins/jenkins:2.277.1-lts-centos7"
}

resource "docker_volume" "jenkins-data" {
  name = "jenkins-data"
}

resource "docker_container" "jenkins" {
  image = docker_image.jenkins.latest
  name  = "jenkins"

  volumes {
    volume_name = "jenkins-data"
    container_path = "/var/jenkins_home"
  }
  networks_advanced {
    name = "web"
  }
  networks_advanced {
    name = "socket-private"
  }

  labels {
    label = "traefik.enable"
    value = true
  }
  labels {
    label = "traefik.http.routers.jenkins.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.jenkins.rule"
    value = "host(`jenkins.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.jenkins.service"
    value = "jenkins"
  }
  labels {
    label = "traefik.http.routers.jenkins-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.jenkins-ssl.rule"
    value = "host(`jenkins.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.jenkins-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.jenkins-ssl.service"
    value = "jenkins"
  }
  labels {
    label = "traefik.http.services.jenkins.loadBalancer.server.port"
    value = "8080"
  }
}
