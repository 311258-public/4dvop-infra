resource "docker_image" "splunk" {
  name = "splunk/splunk:8.1.3-debian"
}

resource "docker_volume" "splunk-data" {
  name = "splunk-data"
}

resource "docker_volume" "splunk-config" {
  name = "splunk-config"
}

resource "docker_container" "splunk" {
  image = docker_image.splunk.latest
  name  = "splunk"

  volumes {
    volume_name = "splunk-data"
    container_path = "/opt/splunk/var"
  }
  volumes {
    volume_name = "splunk-config"
    container_path = "/opt/splunk/etc"
  }
  networks_advanced {
    name = "web"
  }

  env = [
    "SPLUNK_PASSWORD=password",
    "SPLUNK_START_ARGS=--accept-license"
  ]

  labels {
    label = "traefik.enable"
    value = true
  }
  labels {
    label = "traefik.http.routers.splunk.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.splunk.rule"
    value = "host(`splunk.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.splunk.service"
    value = "splunk"
  }
  labels {
    label = "traefik.http.routers.splunk-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.splunk-ssl.rule"
    value = "host(`splunk.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.splunk-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.splunk-ssl.service"
    value = "splunk"
  }
  labels {
    label = "traefik.http.services.splunk.loadBalancer.server.port"
    value = "8000"
  }
  labels {
    label = "traefik.http.routers.splunk-collect.entryPoints"
    value = "web"
  }
  labels {
    label = "traefik.http.routers.splunk-collect.rule"
    value = "host(`splunk-collect.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.splunk-collect.service"
    value = "splunk-collect"
  }
  labels {
    label = "traefik.http.routers.splunk-collect-ssl.entryPoints"
    value = "web-secure"
  }
  labels {
    label = "traefik.http.routers.splunk-collect-ssl.rule"
    value = "host(`splunk-collect.northamp.local`)"
  }
  labels {
    label = "traefik.http.routers.splunk-collect-ssl.tls"
    value = true
  }
  labels {
    label = "traefik.http.routers.splunk-collect-ssl.service"
    value = "splunk-collect"
  }
  labels {
    label = "traefik.http.services.splunk-collect.loadBalancer.server.scheme"
    value = "https"
  }
  labels {
    label = "traefik.http.services.splunk-collect.loadBalancer.server.port"
    value = "8088"
  }
}
